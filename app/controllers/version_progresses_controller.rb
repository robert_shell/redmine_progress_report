class VersionProgressesController < ApplicationController
  unloadable

  before_filter :require_admin

  include VersionProgressHelper
  helper :version_progress

  def index
    @project = params[:project_id] ? Project.find(params[:project_id]) : Project.first
    redirect_to version_progress_url :action=>:show,:id=>@project.id, :version_id => params[:version_id] if @project
  end

  def show
    @project = Project.find(params[:id])
    @version = @project.versions.exists?(params[:version_id]) ? Version.find(params[:version_id]) : @project.versions.first

    @projects = Project.visible.all(:order => 'name asc')
    @versions = Version.all(:conditions => ["status = 'open' and project_id =?", @project.id], :order => 'name asc')

    from_date = 0
    to_date = DateTime.now

    if params[:period] == 'from_date'
      flash.now[:warning] = "Please enter a date." if params[:from_date].blank?
      from_date = params[:from_date]
    elsif params[:period] == 'between'
      flash.now[:warning] = "Please enter a date range." if params[:from_date].blank? || params[:to_date].blank?
      from_date = params[:from_date]
      to_date = params[:to_date]
    end

    @version_progresses = @version ? VersionProgress.all(:conditions => ["version_id = ? and created_at > ? and created_at < ?", @version.id, from_date, to_date], :order => "created_at desc") : nil

    logger.info "commit is #{params[:commit]}"

    if params[:commit] == "Export to CSV"
      send_data(version_progresses_to_csv(@version_progresses), :type => 'text/csv; header=present', :filename => "export_#{DateTime.now.strftime('%Y-%m-%d_%H-%M')}.csv")
    end

  end

  def record_progress
    VersionProgress.record_progress
    flash.now[:notice] = "Recorded progresses on all active versions."
    redirect_to version_progresses_url
  end

end
