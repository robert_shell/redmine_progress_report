# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

resources :version_progresses, :only=> [:index, :show] do
  get 'record_progress', :on => :collection
end